package com.example.michalmatusiak.testanimacji;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView image;
    private Button button;
    private Button retry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = (ImageView) findViewById(R.id.imageView);
        button = (Button) findViewById(R.id.buttonStart);
        retry = (Button) findViewById(R.id.buttonRetryy);
    }

    public void buttonClick(View view) {
        image.setVisibility(View.VISIBLE);
        button.setVisibility(View.INVISIBLE);

        ObjectAnimator slide = ObjectAnimator.ofFloat(image, "translationY", -300f, 0f);
        ObjectAnimator alpha = ObjectAnimator.ofFloat(image, "alpha", 0f, 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(alpha, slide);
        animSetXY.setDuration(500);
        animSetXY.setInterpolator(new DecelerateInterpolator());
        animSetXY.start();

        new Handler()
                .postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                retry.setVisibility(View.VISIBLE);
                                ObjectAnimator slideButton = ObjectAnimator.ofFloat(retry, "translationY", -DeviceManager.getScreenHeight(getBaseContext()) / 8, 0f);
                                ObjectAnimator alphaButton = ObjectAnimator.ofFloat(retry, "alpha", 0f, 1f);
                                AnimatorSet anim = new AnimatorSet();
                                anim.playTogether(alphaButton, slideButton);
                                anim.setInterpolator(new DecelerateInterpolator());
                                anim.setDuration(500);
                                anim.start();
                            }
                        }, 200);
    }

    public void retryClick(View view) {
        image.setVisibility(View.INVISIBLE);
        button.setVisibility(View.VISIBLE);
        retry.setVisibility(View.INVISIBLE);
    }
}
